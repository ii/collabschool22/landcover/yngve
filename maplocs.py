import numpy as np
from matplotlib import pyplot as plt, patches
# TODO:
# 1. Define function that takes in coordinates 
# 2. Find out how to determine whether it is land or water

data_width = 43200
data_height = 21600

# Load data as 1D
mm = np.memmap('gl-latlong-1km-landcover.bsq')

# Reshape to 2D
mm2d = mm.reshape((data_height, data_width))

def scale1d(old_range, new_range):
  dmin, dmax = old_range
  rmin, rmax = new_range

  return lambda v: (((v - dmin) * (rmax - rmin)) / (dmax - dmin)) + rmin

scale_ny = scale1d([-90, 90], [0, data_height])
scale_ex = scale1d([-180, 180], [0, data_width])
def ne_to_xy(n, e):
  return round(scale_ex(e)), round(scale_ny(n))

def infer_type(n, e):
  x, y = ne_to_xy(n, e)
  value = mm2d[y, x]

  if value == 0:
    print('Water!')
  else:
    print('Not water')

def render(n, e):
  data_to_show = mm2d[::50,::50]
  render_width = len(data_to_show[0])
  render_height = len(data_to_show)

  scale_n = scale1d([-90, 90],[0, render_height])
  scale_e = scale1d([-180, 180],[0, render_width])

  cx = scale_e(e)
  cy = scale_n(n)

  w = scale_e(10-180)
  h = scale_n(10-90)

  fig, ax = plt.subplots(1)
  show = ax.imshow(mm2d[::50,::50])
  ax.add_patch(patches.Rectangle((cx - w/2, cy - h/2), w, h, linewidth=1, edgecolor='r', facecolor='none'))

render(45, 45)
infer_type(45, 45)